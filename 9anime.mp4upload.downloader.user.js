    // ==UserScript==
    // @name         9anime mp4upload downloader
    // @version      1.0
    // @description  9anime downloader
    // @author       Miosame
    // @match        https://9anime.to/watch/*
    // @grant        none
    // ==/UserScript==

    (function() {
        'use strict';
        let lastMessage, messageNow;
        let oldXHR = window.XMLHttpRequest;
        let linksArr = [];
        let episodeCount = 0;
        let currentPage = 0;
        let scraping = false;

        function waitForElem(selector,callback) {
            if (document.querySelector(selector)) callback(document.querySelector(selector));
            let checkExist = setInterval(() => {
                if (document.querySelector(selector)) {
                    clearInterval(checkExist);
                    callback(document.querySelector(selector));
                }
            }, 100);
        }

        function fetchAnime() {
            document.getElementsByTagName("iframe")[0].addEventListener("load", async () => {
                let animeID = document.getElementsByTagName("iframe")[0].src.replace("https://www.mp4upload.com/embed-","").replace(".html?autostart=true","");
                messageNow = "https://www.mp4upload.com/" + animeID;

                if(animeID && (lastMessage !== messageNow) && !linksArr.includes(messageNow)) {
                    linksArr.push(messageNow);
                    console.log("Download (", linksArr.length, ") ", linksArr);
                    lastMessage = messageNow;
                }

                nextPage();
            });
        }

        const mp4upload_container = "div#servers-container div.widget.servers div.widget-body div.server:nth-of-type(3)";
        function fetchEpisodesCount() {
            let allElems = document.querySelector(mp4upload_container).querySelectorAll("li a");
            let activeElem = document.querySelector("a.active");
            let episodes = allElems.length;

            currentPage = Array.prototype.slice.call(allElems).indexOf(activeElem);
            currentPage++;
            return episodes;
        }

        function nextPage() {
            if(currentPage === episodeCount){
                var textArea = document.getElementById("textAreaPaste");

                if (!textArea){
                    textArea = document.createElement("textarea");
                    textArea.id = "textAreaPaste";
                    textArea.cols = "40";
                    textArea.rows = linksArr.length;
                }

                textArea.value = linksArr.join("\n");
                document.body.prepend(textArea);

                scraping = false;
                return;
            }

            let allElems = document.querySelector(mp4upload_container).querySelectorAll("li a");
            allElems.item(currentPage).click();
        }

        function execT() {
            // wait for video iframe
            waitForElem("iframe:nth-of-type(1)", (elem) => {
                fetchAnime(elem);

                // wait for episode list
                waitForElem(mp4upload_container, (elem) => {
                    episodeCount = fetchEpisodesCount();
                });
            });
        }

        function newXHR() {
            var realXHR = new oldXHR();
            realXHR.addEventListener("readystatechange", function() {
                if(realXHR.readyState==4 && realXHR.status==200 && scraping){
                    execT();
                }
            }, false);
            return realXHR;
        }

        function createButton(label, onClick) {
            var btn = document.createElement("BUTTON");
            btn.innerHTML = label;
            btn.onclick = onClick;
            document.body.prepend(btn);
        }

        function initScrapeButton() {
            createButton("Scrape it!", () => {
                scraping = true;
                execT();
            });
        }

        function initTraktButton() {
            let anime_title = document.querySelector(".title").textContent.replace(" (Dub)","").toLowerCase();
            createButton("Trakt", () => {
                window.location.href = "https://trakt.tv/search/shows?query=" + encodeURI(anime_title);
            });
        }

        function init() {
            //initTraktButton();
            initScrapeButton();
            window.XMLHttpRequest = newXHR;
        }

        init();
    })();