// ==UserScript==
// @name         AnimeUltima Scraper
// @version      1.1
// @author       Miosame
// @match        https://*.animeultima.to/a/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    const episodeSelector = "html#main-html body div#app section.section div.container.is-fullhd div.columns div.column.is-three-quarters div.box-item.column-item div div table.table.is-fullwidth.is-hoverable.is-narrow-mobile tbody tr span a";

    function waitForElem(selector,callback) {
        if (document.querySelector(selector)) callback(document.querySelector(selector));
        let checkExist = setInterval(() => {
            if (document.querySelector(selector)) {
                clearInterval(checkExist);
                callback(document.querySelector(selector));
            }
        }, 100);
    }

    const titleSelector = "html#main-html body div#app section.section div.container.is-fullhd div.columns div.column.is-three-quarters h2.title.is-size-4";

    function showLinks(links) {
        let textArea = document.getElementById("textAreaPaste");

        if (!textArea){
            textArea = document.createElement("textarea");
            textArea.id = "textAreaPaste";
            textArea.cols = "80";
            textArea.rows = links.length;
        }

        textArea.value = links.join("\n");
        document.querySelector(titleSelector).append(textArea);
    }

    function spawnProgress(max) {
        let progress = document.getElementById("progressBar");

        if (!progress){
            progress = document.createElement("progress");
            progress.id = "progressBar";
            progress.max = max;
        }

        progress.value = 0;
        document.querySelector(titleSelector).append(document.createElement("br"));
        document.querySelector(titleSelector).append(progress);
    }

    function incrementProgress() {
        const progress = document.getElementById("progressBar");
        progress.setAttribute("value", progress.value + 1);

        // remove progressbar if finished
        if(progress.value === progress.max) progress.outerHTML = "";
    }

    let downloaded = 0;
    async function fetchURL(allElems, episode, episodes, seriesID){
        const thisEpisodeLink = allElems[episode].href;
        const failed = false;

        // fetch series ID
        const response = await fetch(thisEpisodeLink).catch(async () => {
            failed = true;
        });

        if(response.ok && failed === false) {
            const data = await response.text();
            const seriesFileID = /<input type="hidden" value="([0-9]+)" name="commentable_id">/.exec(data)[1];
            const dubSub = ((allElems[episode].parentNode.parentNode.parentNode.querySelector(".dub-icon")) ? "dub" : "sub");
            let downloadURL = "https://storage.googleapis.com/auengine.appspot.com/"+seriesID+"/"+dubSub+"/"+(episodes - episode)+"_"+seriesFileID+".mp4";

            let testFileURL = await fetch(downloadURL);
            if(!testFileURL.ok) {
                // force sub and try again
                const testSubURL = "https://storage.googleapis.com/auengine.appspot.com/"+seriesID+"/sub/"+(episodes - episode)+"_"+seriesFileID+".mp4";
                const testSubResult = await fetch(testSubURL);

                if(testSubResult.ok) {
                    downloadURL = testSubURL;
                }else{
                    // TODO: cancel all the other requests?
                }
            }

            incrementProgress();
            return downloadURL;
        }else{
            // cooldown for 500ms
            setTimeout(async () => {
                await fetchURL(allElems, episode, seriesID)
            }, 500);
        }
    }

    waitForElem(episodeSelector, async (elem) => {
        // TODO: old method was just incrementing the seriesfileID
        // which was way faster and worked for most anime, just check for 404 on the resulting file url and fallback
        // to the more expensive method, if it returned a 404 for the incremented ID - then increment from that new found offset

        // EDIT: the above todo might not be it, chief, the filenames seem to be random enough on a lot of anime
        // weird that it worked at all

        const seriesID = /cdn.animeultima.tv\/cover-photo\/([0-9]+)/.exec(document.querySelector(".thumbnail").src)[1];
        const allElems = document.querySelectorAll(episodeSelector);
        const episodes = allElems.length;
        spawnProgress(episodes);

        const links = [];
        const queue = [];
        for (let episode = 0; episode < episodes; episode++) {
            queue.push(async () => {
                links.push(await fetchURL(allElems, episode, episodes, seriesID));
            });
        }

        while (queue.length) {
            // 5 at at time
            await Promise.all( queue.splice(0, 5).map(f => f()) )
        }

        links.sort(function(a, b){
            // get filenames only
            a = a.substring(a.lastIndexOf('/')+1)
            b = b.substring(b.lastIndexOf('/')+1)

            // match only the episode #
            a = /([0-9]+)_/.exec(a)[1]
            b = /([0-9]+)_/.exec(b)[1]

            return a - b
        });

        showLinks(links);
    });
})();